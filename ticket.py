# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import PYSONEncoder
from trytond.pool import Pool


class Ticket(ModelSQL, ModelView):
    _name = 'ticket.work'

    def __init__(self):
        super(Ticket, self).__init__()
        self._error_messages.update({
            'no_pricelist': 'No pricelist found for party "%s", code "%s"!',
            'no_contract': 'No support contract for party %s, code "%s"!',
            'error_compute_billing_line': 'Computing billing line %s fails.',
        })

    def get_invoicing_party(self, ticket):
        return ticket.requestor

    def get_pricelist(self, ticket):
        res = False
        if ticket.requestor and ticket.requestor.contract_support:
            res = ticket.requestor.contract_support.pricelist
        return res

    def get_contract(self, ticket):
        res = False
        if ticket.requestor:
            res = ticket.requestor.contract_support
        return res

    def _compute_billing_line_vals(self, line, defaults):
        pricelist_obj = Pool().get('pricelist.pricelist')

        res = []
        if not defaults.get('contract'):
            self.raise_user_error('no_contract', error_args=(
                defaults['party'].full_name, defaults['party'].code))
        if not defaults.get('pricelist'):
            self.raise_user_error('no_pricelist', error_args=(
                defaults['party'].full_name, defaults['party'].code))
        billing_vals = pricelist_obj.compute_billing_lines(
                defaults['product'].id, quantity=line.hours,
                from_unit=defaults['from_uom'],
                pricelist=defaults['pricelist'],
                start_time=line.start_time, end_time=line.end_time)
        if not billing_vals:
            self.raise_user_error('error_compute_billing_line',
                    error_args=(line.description,))

        for value in billing_vals:
            if not value.get('quantity'):
                continue
            billing_line = {}
            billing_line['product'] = defaults['product'].id
            billing_line['party'] = defaults['party'].id
            # TODO: which version is better?
            #billing_line['billing_date'] = line.start_time.date()
            billing_line['billing_date'] = value['date']
            billing_line['quantity'] = value['quantity']
            billing_line['unit_price'] = value['price']
            billing_line['unit'] = value['unit']
            billing_line['contract'] = defaults['contract'] \
                    and defaults['contract'].id or False
            billing_line['description'] = value['description'] \
                    or defaults['product'].name
            billing_line['state'] = 'draft'
            res.append(billing_line)
        return res

Ticket()


class ConfirmBillingInfo(ModelView):
    'Confirm Billing Info'
    _name = 'ticket.work.confirm_billing.info'
    _description = __doc__

    success_confirm = fields.Integer('Tickets confirmed successfull',
        readonly=True)

ConfirmBillingInfo()


class ConfirmBilling(Wizard):
    'Confirm Billing'
    _name = 'ticket.work.confirm_billing'
    states = {
        'init': {
            'actions': ['_confirm'],
            'result': {
                'type': 'form',
                'object': 'ticket.work.confirm_billing.info',
                'state': [
                    ('end', 'OK', 'tryton-ok'),
                ],
            },
        },
    }

    def _confirm(self, data):
        billing_line_obj = Pool().get('account.invoice.billing_line')
        ticket_obj = Pool().get('ticket.work')

        i = 0
        for ticket in ticket_obj.browse(data['ids']):
            if ticket.state != 'closed':
                continue
            for timesheet_line in ticket.timesheet_lines:
                billing_lines = [x.id for x in timesheet_line.billing_lines
                    if x.state == 'draft']
                billing_line_obj.write(billing_lines, {
                    'state': 'confirmed',
                })
            i += 1
        return {'success_confirm': i}

ConfirmBilling()


class OpenBillableTickets(Wizard):
    'Open Billable Tickets'
    _name = 'ticket.work.open_billable_tickets'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'action',
                'action': '_action_open_billable_tickets',
                'state': 'end',
            },
        },
    }

    def _action_open_billable_tickets(self, data):
        pool = Pool()
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')
        ticket_obj = pool.get('ticket.work')
        billing_line_obj = pool.get('account.invoice.billing_line')

        args = [('work.timesheet_lines', '=', True)]
        tickets = ticket_obj.search_read(args,
            fields_names=['work.timesheet_lines'])
        timesheet_line_ids = []
        timesheet_line2ticket = {}
        for ticket in tickets:
            timesheet_line_ids += ticket['work.timesheet_lines']
            for line in ticket['work.timesheet_lines']:
                timesheet_line2ticket[line] = ticket['id']

        args = [
            ('timesheet_line', 'in', timesheet_line_ids),
            ('state', '=', 'draft'),
            ]
        billing_lines = billing_line_obj.search_read(args,
            fields_names=[('timesheet_line')])
        timesheet_lines_with_draft_billing_lines = []
        for line in billing_lines:
            timesheet_lines_with_draft_billing_lines.append(
                line['timesheet_line'])

        tickets_to_view = []
        for line in timesheet_lines_with_draft_billing_lines:
            if timesheet_line2ticket.get(line):
                tickets_to_view.append(timesheet_line2ticket[line])

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_ticket_form_to_bill'),
            ('module', '=', 'ticket_billing'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        domain = PYSONEncoder().encode([
            ('id', 'in', tickets_to_view),
            ])
        if res['pyson_domain'] != '[]':
            res['pyson_domain'] = (res['pyson_domain'][:-1] + ', ' +
                domain[1:-1] + ']')
        else:
            res['pyson_domain'] = domain
        return res

OpenBillableTickets()
