# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Ticket Billing',
    'name_de_DE': 'Ticket Abrechnungszeilen',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds the possibility to create billing lines for tickets
    ''',
    'description_de_DE': '''
    - Fügt die Möglichkeit der Erstellung von Abrechnungszeilen für Tickets
    hinzu.
    ''',
    'depends': [
        'account_invoice_pricelist_extracharge',
        'contract_billing_pricelist',
        'ticket',
        'timesheet_billing_contract',
        'timesheet_billing_pricelist',
    ],
    'xml': [
        'party.xml',
        'ticket.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
